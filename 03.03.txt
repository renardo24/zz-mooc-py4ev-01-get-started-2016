So we're almost done with conditionals. I want to show you one
more kind of conditional. It's a little bit different. It's not a bit of code
structure that you make, it is dealing with the fact
that some things may blow up. Like if you read a number from a user and
you try to convert it to a floating point number, as you may have already done
in some of your homework, it can blow up. You know it's going to blow up, but you
don't exactly want to kill your program. So the concept of try and except are,
hey, this is a dangerous thing. I know it might blow up,
I know exactly what it might blow up, but I don't want to die, and I don't want
to stop my program when it blows up. I want to continue and
that's the purpose of the except block. So here's a little bit of code, and
you know we've done this code before. This is code that's kind of similar
to your rate and pay homework, where you read a string using raw input,
you converted it using float, but then if you typed in Fred,
the thing blows up. So we're kind of simulating
that right here. So here we have a variable
a string called Hello Bob, and we try to turn it into an integer,
and then we're going to print that out. And then we have another string that
has the letters one, two, three. You convert that to an integer,
and then we print that one out. The problem is, is that when this runs,
this is going to fail. It's going to fail with this traceback. Okay? And the problem is, is when the traceback
happens, the program stops executing. The traceback is Python's way of
asking you, hey, this would be bad. I don't know what to do. I'm stopping. So that means that the rest
of your program is gone. Right?
The fact that we had stuff down here doesn't matter. This line died with the traceback. It stopped. It doesn't like give you a traceback and
then keep going. It gives you a traceback and
that's the end. Now this might be something,
instead of just the string hello Bob, which is insane, data might have come from
a raw input, where the user was typing, and you're saying, give me a number. And they type something that's not
a number, and this would blow up. It's like, hey I know it's going to blow up. The problem with this is that you don't,
oops, clear the thing. Now we have to start it on fire again. Okay, it's on fire. The problem is, is that in a sense,
this program is you. If you recall, we have you as the typing
these commands into these scripts, feeding the central processing unit,
answering the question, what next? So you should take it a little
personally when your program gets a traceback because that means you,
in the form of your program, have been vaporized and you're not
present to give any more instructions. It stops. It stops dead in its tracks. You are gone. So, we want to make sure
we control this behavior. We know it might blow up, and we want to capture the situation where
it does and execute alternate code. Okay, so here it goes. It's a bit of syntax. I mentioned that it uses the try and
except key words. These are reserved words in Python. And
then it's a little indented block. So astr='hello Bob', great. Try means we're about to
do something dangerous, let's take out some
insurance policy on it. And that is, we are going to
convert this to an integer. Take astr, convert to an integer,
put it in istr. If that works, great, we'll just
continue on and ignore this except. If it blows up, we're going to 
jump into the except block and then we'll have alternate,
substitute code. In this case, I'm going to set the variable
to negative one as an indicator. Then I'll print it out. And I'll do it again. Try this code, and away we go. So, when this runs,
we know exactly how it's going to run. It- Come back. We'll set this string. The try takes out the insurance. This blows up, so
it runs down to here and runs this part. And then it'll print, First -1. And it sets the string to one, two, three,
not 123, but one, two, three, as a string. It takes out an insurance policy. This time it works and that puts,
istr is going to be 123, so we don't run the except code, and so
out comes, the second, one, two, three. Okay? So the try is, take out insurance
on this little bit of code, and if it fails, run this alternate code. If not, skip the alternate code. So it's kind of conditional. If you put multiple lines in
the block between the try and the except, it runs until one dies. So it doesn't come back. Okay, it's not taking insurance out
separately on all three statements, it's like, here's a block of stuff and
if anything blows up, stop. And the things that run do run, so, if this is really kind of bad code because
you really don't want the print in here. It's actually a good idea in the try,
except to have as little in the try block as you possibly can so
you're real clear on what's going to fail. So here we come in [SOUND]. It's Bob, so it's going to fail. We run this, that runs successfully,
this blows up, so it quits and jumps into
the except blocks and continues. The point is,
is that this code never executes. Never executes. The other point is,
this code does execute. Just because this blew up,
this is already executed. It might have done something other,
more complex than print hello, okay? So there you go. So, if we look at this kind of in
a picture, we set the try block. It runs. It runs. And the try / except kind of has
this escape hatch that says, if there is a [SOUND] explosion somehow, then it runs this alternate code and
it comes out and finishes. Okay? And again, it doesn't go back and
finish the block and it doesn't undo the work that is done
by that, so it doesn't un-execute it. If it executes and
works it just keeps on going, then it blows up,
then it sort of flushes its way out. Okay? So here's an example of how you
might do this in a running program, like the programs that
you're about to be assigned, where you're supposed to check for
user input having errors. So here is a little
conversion of a number, and so we're saying, enter a number and
we're putting a string into rawstr. It's a string and so we don't know. And here's where we're going to
convert it to an integer and we're just not sure if it's going to 
work or not. So we know how int works. It either converts it or it blows up. So we know it's going to do that, we just
don't know what the user's going to type. We don't know. So we have to take out insurance on it. So this runs, and then we do a try and
then we try to convert it. And if it works, it's great, and
if it fails, it runs this and sets it to negative one. And afterwards,
we either have the number or negative one. And so if the person enters 42,
it says, nice work. Well, let's show you where it runs. If the person says 42 it runs
through here, gets the string 42, converts that to an integer,
skips here, and then says, nice work. And that's how it runs. If on the other hand they type forty two,
the words, this gets to be the string 'forty two'. It runs here, this blows up. So it comes and runs this part here, and
then it says if ival is greater than zero, which it is not true, so
it runs this part, and says not a number. So this is our way of compensating for user input that might have errors in it,
errors that were anticipated, right? You'd rather at least put
up some kind of a message rather than just have a traceback if
you're writing code for somebody else. It just kind of is not very classy. So the classic program to do this is a time and
a half for overtime pay. So you get some pay rate, like $10
an hour for your first 40 hours, and then you get $15 for any hours above it. So you have to say, oh, okay,
if this ends up being some kind of a thing where,
let me draw that picture a little better. Hours greater than 40,
you're going to do one thing, and if hours are less than 40
you're going to do another thing. So you have two payout calculations. If the hours are greater than 40,
then you're going to do overtime calculation, which is kind of like 40 times the regular
rate and then the number of excess hours, like five overtime hours, times
the rate times one and a half. So this is kind of the calculation that
happens if the hours are greater than 40. And then, if the hours are less than 40,
it's just pay equals rate times hours. So you're going to do one of two
calculations depending on how it works. So that's one of the programming
problems for this chapter. That's a classic,
it's the classic if / then / else. You can actually do it with
an if / then if you're tricky. There's a lot of ways to do this,
so pick one and do it. Now the next thing I want you to do is
I want you to take that same program, do it again in another assignment, or
another problem in the chapter, and that is have some kind of a non-numeric
input and have it blow up. So if they type something like nine, put out an error, or if they type
something here, put out an error. Now don't write a loop, no loop. This is one execution of the program and
this is another execution of the program. Later, we can write loops,
we haven't even talked about loops. So this is running it twice. All I want you to do is exit. So take a look in the book
as to how to just get out. So I don't want you to try to say,
I'm going to prompt for these numbers until I get a good one. We'll do that later. I just want you to deal with
the fact that you read a thing, you use the try to convert it
to a float and see if it works. And if you don't, you just quit. Don't try to be tricky and
repeatedly prompt. So don't repeatedly prompt. One prompt and then quit. Okay? So this is conditional execution. If, if / then / else, and then I added a little bit with the try and
except as well. And the try and except is really
a limited kind of a problem. It really is to compensate for errors that
you anticipate are going to happen and you can imagine what you want to do as
a replacement for what those errors are. Okay?
See you next lecture.